package com.example.lab01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    // 1 se declara los componentes
    EditText txtNom;
    EditText txtApe;
    Button btnIng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 2 obtener los componentes
        txtNom = findViewById(R.id.txtNombre);
        txtApe = findViewById(R.id.txtApellido);
        btnIng = findViewById(R.id.btnIngresar);

        //3 se programa los eventos
        btnIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = txtNom.getText().toString();
                String ape = txtApe.getText().toString();
                mensaje("Bienvenido : "+ nom + " " + ape);

                Intent intent = new Intent(MainActivity.this, CatalogoActivity.class);
                startActivity(intent);
            }
        });
    }

    void mensaje(String mensaje){
        Toast toast1 = Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG);
        toast1.show();
    }
}
